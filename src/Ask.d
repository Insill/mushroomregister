module mushroomregister.ask;

import std.stdio;
import std.conv;
import std.string;
import std.datetime;
import std.datetime.date;

class Ask {
public:

	static int askInteger() {
		bool valid = true;
		int number;

		do {
			try {
				string input = stdin.readln().strip;
				number = to!int(input);
				valid = true;
			} catch (Exception e) {
				writeln("Bad input! Give an integer.");
				valid = false;
			}
		}
		while (!valid);

		return number;
	}

	static double askDouble() {
		bool valid = true;
		double number = 0;

		do {
			try {
				string input = stdin.readln().strip;
				number = to!double(input);
				valid = true;
			} catch (Exception e) {
				writeln("Bad number! Give a decimal number");
				valid = false;
			}
		}
		while (!valid);
		return number;
	}

	static char askChar() {
		bool valid = true;
		char character = '0';

		do {
			try {
				string input = stdin.readln().strip;
				character = to!char(input);
			} catch (Exception e) {
				writeln("Bad input! Give a character.");
				valid = false;
			}
		}
		while (!valid);
		return character;
	}

	static bool askBoolean() {
		char character;
		bool valid = false;

		do {
			string input = stdin.readln().strip;
			character = to!char(input);
			if (character == 'y' || character == 'n') {
				valid = true;
			} else {
				writeln("Bad input! Give y for yes or n for no.");
			}
		}
		while (!valid);

		if (character == 'y')
			return true;

		return false;
	}

	static string askString() {
		string input = stdin.readln().strip;
		return input;
	}
	/*
	public static bool checkCollectionDate(int day, int month, int year, Date date) {
		if (assert(!valid!"days"(year,month,day))) {
			return false; 
		}
		if (assert(!valid!"months"(year,month,day))) {
			return false;
		}
		if (day != date.day) {
			return false; 
		}
		if (month != date.month) {
			return false; 
		}
		if(year != date.year) {
			return false; 
		}
		return true; 
	} 
*/

}
