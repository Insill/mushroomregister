module mushroomregister.mushroomlist;

import std.stdio;
import std.datetime;
import mushroomregister.mushroom;
import msgpack;

public class MushroomList {

public:

	Mushroom listFirst;

	this() {
		listFirst = null;
	}

	string addMushroom(Mushroom newMushroom) {
		if (newMushroom is null) {
			return "Empty reference!";
		}
		newMushroom.setNext(listFirst);
		listFirst = newMushroom;
		writeln("\nA new mushroom has been added!\n");
		return "";
	}

	string searchMushroomsPoison(int poison) {
		Mushroom printable = listFirst;
		string allMushrooms = " ";
		while (printable !is null) {
			if (poison == printable.getPoisonRating()) {
				allMushrooms = printable.toString ~ "\n" ~ allMushrooms;
				printable = printable.getNext();
			}
			if (allMushrooms == "") {
				writeln("No mushrooms were found according to the chosen
						poison rate");
				return allMushrooms;
			}
		}
		return "";
	}

	string searchMushroomsSpecies(string species) {
		Mushroom printable = listFirst;
		string allMushrooms = "";
		while (printable !is null) {
			if (species == printable.getSpecies()) {
				allMushrooms ~= printable.toString ~ "\n" ~ allMushrooms;
				printable.getNext();
			}
		}
		if (allMushrooms == "") {
			writeln("No mushrooms were found!");
		}
		return allMushrooms;
	}

	string deleteMushroom(string species, int day, int month, int year) {
		bool deleted = false;
		Date date;

		if (listFirst is null) {
			return "Deletion not successful, the list is empty!";
		}

		while (listFirst !is null) {
			date = listFirst.getCollectionDate();
			if (date.year == year && date.month == month && date.day == day
				&& species == listFirst.getSpecies()) {
				writeln("Deleting\n", listFirst);
				listFirst = listFirst.getNext();
				deleted = true;
			} else {
				break;
			}
		}
		Mushroom target = listFirst;
		while (target !is null && target.getNext() !is null) {
			date = target.getNext().getCollectionDate();
			if ((date.year == year && date.month == month - 1
					&& date.day == day) && species == target.getSpecies()) {
				target.setNext(target.getNext().getNext());
				deleted = true;
			} else {
				target = target.getNext();
			}
		}
		if (!deleted) {
			return "The mushroom to be deleted was not found.\n";
		} else {
			return "";
		}
	}

	string printMushrooms() {
		Mushroom printable = listFirst;
		string allMushrooms = "";
		while (printable !is null) {
			allMushrooms ~= printable.toString ~ "\n";
			printable = printable.getNext();
		}
		return allMushrooms;
	}

}
