module mushroomregister.mushroom;

import std.stdio;
import std.datetime;
import std.conv;
import std.string;

class Mushroom {

private:
	string species;
	int amount;
	int poisonRating;
	Date collectionDate;
	Mushroom next;

public:
	this() {
		throw new Exception("");
	}

	this(string species, int amount, int poisonRating, Date date) {
		string error = setSpecies(species);
		error ~= setAmount(amount);
		error ~= setCollectionDate(date);
		error ~= setPoisonRating(poisonRating);

		if (error.length > 0) {
			throw new Exception(error);
		}
	}

	string setSpecies(string newSpecies) {
		if (newSpecies != null && newSpecies.length > 0 && !isNumeric(newSpecies)) {
			this.species = newSpecies;
			return "";
		}

		return "The name cannot be blank!\n";
	}

	string getSpecies() {
		return species;
	}

	void setNext(Mushroom reference) {
		next = reference;
	}

	Mushroom getNext() {
		return next;
	}

	string setAmount(int new_amount) {
		if (new_amount < 0) {
			return "The amount cannot be negative!";
		}

		if (new_amount == 0) {
			return "The amount cannot be zero!";
		}

		amount = new_amount;
		return "";
	}

	int getAmount() {
		return amount;
	}

	string setCollectionDate(Date newDate) {
		if (newDate.toString == "") {
			return "The collection date is not valid!\n";
		}
		SysTime current = Clock.currTime();
		Date currentDate = Date(current.year, current.month, current
				.day);
		Date oldDate = Date(1900, 1, 1);

		if (newDate < oldDate) {
			return "The collection date is invalid,
				it's too far back in the past!";
		}
		if (newDate > currentDate) {
			return "The collection date is invalid,
				it's in the future!";
		}
		collectionDate = newDate;
		return "";
	}

	Date getCollectionDate() {
		return collectionDate;
	}

	string setPoisonRating(int new_poison) {
		if (new_poison > 0 && new_poison < 4) {
			poisonRating = new_poison;
			return "";
		}
		return "The poison rating has to be between 1-3!\n
			(1 - not poisonous) \n(2 - moderately poisonus)
			\n(3 - lethal)";
	}

	int getPoisonRating() {
		return poisonRating;
	}

	static bool checkSpecies(
		string species) {
		if (species != null && species.length > 0) {
			return true;
		}
		return false;
	}

	override string toString() {
		string all = "";
		all ~= this.species ~ " \n";
		all ~= "Amount: " ~ to!string(
			this.amount) ~ ", ";
		all ~= "Poison rating: " ~ to!string(
			this.poisonRating) ~ ", ";
		all ~= "Collection date: " ~ this
			.collectionDate.toString();
		return all;
	}

}
