module mushroomregister.mushroomregister;

import std.stdio;
import std.file;
import std.datetime;
import std.string;
import msgpack;
import mushroomregister.mushroom;
import mushroomregister.mushroomlist;
import mushroomregister.ask;

class MushroomRegister {

private:
	bool saved;
	bool changed;
	MushroomList list;
	Mushroom mushroom;

public:

	void main() {
		start();
		int choice = 0;

		do {
			choice = menu();
			switch (choice) {
			default:
				writeln("");
				break;
			case 0:
				exit();
				break;
			case 1:
				addMushroom();
				break;
			case 2:
				deleteMushroom();
				break;
			case 3:
				search();
				break;
			case 4:
				print();
				break;
			case 5:
				openMushroomList();
				break;
			case 6:
				if (list.listFirst !is null) {
					saveMushroomList();
				}
				break;
			}
		}
		while (choice > 0);
	}

	void start() {
		list = new MushroomList();
		changed = false;
	}

	int menu() {
		int choice = 0;
		do {
			writeln("\n-------------\nMUSHROOM REGISTER\n-------------\n");
			writeln("1. Add mushroom ");
			writeln("2. Delete mushroom ");
			writeln("3. Search mushrooms ");
			writeln("4. Print mushrooms ");
			writeln("5. Open a mushroom list from a file ");
			writeln("6. Save a mushroom list into a file ");
			writeln("0. Exit \n");
			writeln("Choose your option: ");
			choice = Ask.askInteger();
		}
		while (choice < 0);
		if (choice >= 7) {
			writeln("\n-------------\nWrong choice!
					\n-------------\n");
		}
		return choice;
	}

	void exit() {
		if (changed) {
			writeln("Changes have been made to the list, do you want to save? (Y/N)");
			if (Ask.askBoolean()) {
				saveMushroomList();
				writeln("\n-------------\nBye, welcome again! 
						\n-------------\n");
				return;
			}
		} else {
			writeln("\n-------------\nBye, welcome again!\n-------------\n");
		}
	}

	void search() {
		string species;
		int poisonRate = 0;
		int choice = 0;
		writeln(
			"-------------\nWill the mushrooms searched according to name or their poison rating? 
		\n1 - Name \n2 - Poisonous rating\n0 - Return to the menu \n-------------\n");

		do {
			choice = Ask.askInteger();
			switch (choice) {
			default:
				return;
			case 1:
				write("Give the mushroom's species: ");
				species = Ask.askString();
				if (species.length > 1) {
					writeln(list.searchMushroomsSpecies(species));
				}
				break;
			case 2:
				write("\n(1 - not poisonous)\n(2 - quite poisonous)\n(3 - lethal): ");
				poisonRate = Ask.askInteger();
				if (poisonRate < 4) {
					writeln(list.searchMushroomsPoison(poisonRate));
				}
				break;
			}
		}
		while (choice < 1 || choice > 2);
	}

	void openMushroomList() {
		string name;
		bool proper = true;
		File file;

		do {
			write(
				"-------------\nGive the file's name.\n( Give 0 in order to return to the menu ): ");
			name = Ask.askString();
			if (name == "0") {
				return;
			}
			try {
				file = File(name ~ ".dat", "r");

				if (!exists(name)) {
					proper = false;
					writeln("File not found, give another filename.\n");
				}

				ubyte[] outData = cast(ubyte[]) read(file);
				list = outData.unpack!MushroomList();
				assert(!(list is null));
				assert(!(list.listFirst is null));

			} catch (Exception e) {
				writefln("\n-------------\nERROR:\n%s\n-------------\n", e.toString());
				proper = false;
			}

		}
		while (name.length < 1 || !proper);

		writeln("-------------\nUNDER CONSTRUCTION\n-------------");

		//file.close();
	}

	void saveMushroomList() {
		string name;
		File file;
		do {
			write("-------------\nGive a filename (Give 0 in order to return): ");
			name = strip(Ask.askString());
			if (name == "0") {
				return;
			}

		}
		while (name.length < 1);

		// serialization code here
		try {
			writeln("-------------\nWriting to a file...\n-------------");
			file = File(name ~ ".dat", "wb+");
			ubyte[] inData = pack(list);
			file.rawWrite(inData);

		} catch (Exception e) {
			writef("\n----------------\nERROR:\n%s\n-------------\n", e.toString());
		}
		file.close();
	}

	void addMushroom() {
		string species;

		do {
			write(
				"-------------\nGive the mushroom's species. (Give 0 in case you want to return to the menu):  ");
			species = Ask.askString();
			if (species == "0") {
				return;
			}
		}
		while (!Mushroom.checkSpecies(species));

		write("-------------\nHow many mushrooms have been collected? :");
		int amount = Ask.askInteger();
		writeln(
			"-------------\nHow poisonous is the mushroom?\n(1 - not poisonous)\n(2 - quite poisonous) \n(3 - lethal):");
		int poisonRate = Ask.askInteger();
		write("\n-------------\nGive the collection day: ");
		int day = Ask.askInteger();
		write("-------------\nGive the collection month: ");
		int month = Ask.askInteger();
		write("-------------\nGive the collection year: ");
		int year = Ask.askInteger();
		Date date = Date(year, month, day);
		Mushroom mush = null;
		try {
			mush = new Mushroom(strip(species), amount, poisonRate, date);
		} catch (Exception e) {
			writefln("\n-------------\nERROR:\n%s\n-------------\n", e.toString());
		}

		list.addMushroom(mush);
		saved = false;
		changed = true;
	}

	void deleteMushroom() {
		write(
			"\n-------------\nGive the mushroom's species:\n(Give 0  in case you want to return to the menu) : ");
		string species = Ask.askString();
		if (species == "0") {
			return;
		}
		write("\n-------------\nGive the mushroom's collection year: ");
		int year = Ask.askInteger();
		write("\n-------------\nGive the mushroom's collection day: ");
		int day = Ask.askInteger();
		write("\n-------------\nGive the mushroom's collection month: ");
		int month = Ask.askInteger();

		string status = list.deleteMushroom(species, day, month, year);
		if (status.length > 0) {
			writefln("\n-------------\nERROR:\n%s\n-------------\n", status);
		} else {
			writeln("\n-------------\nDeletion successful!\n-------------\n");
		}
		saved = false;
		changed = true;
	}

	void print() {
		int choice = 0;
		write("\n-------------\nIs the list printed on screen or into a text file?\n1 - On screen 
		\n2 - Into a text file \n0 - Return to menu :\n");

		do {
			choice = Ask.askInteger();
			switch (choice) {
			default:
				writeln("Wrong choice!\n");
				return;
			case 1:
				writeln(list.printMushrooms());
				break;
			case 2:
				write("-------------\nGive the file from which the list will be printed: ");
				writeln("-------------\nUNDER CONSTRUCTION\n-------------");
				break;
			case 0:
				return;
			}
		}
		while (choice < 1 || choice > 2);
	}

	unittest {

	}
}
